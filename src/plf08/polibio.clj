(ns plf08.polibio (:require [plf08.data :as data]))

(defn leer [r] (let [s (slurp r)] s))
(defn escribir [r s] (spit r s))

(defn descifrar [r s] (escribir s (clojure.string/join (map data/tabla-polibio (vec (map (partial apply str) (partition-all 2 (leer r))))))))
(defn cifrar [r s] (escribir s (apply str (map data/tabla-polibio (leer r)))))

(defn init [operacion input output] (cond (or (empty? operacion) (empty? input) (empty? output)) "Parámetros no válidos"
        (= operacion "cifrado") (cifrar input output) (= operacion "descifrado") (descifrar input output) :else "Parámetros no válidos"))
