(ns plf08.transposicion (:require [clojure.string :as string]))
(defn alfabetoMin [] [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z \0 \1 \2 \3 \4 \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9])
(defn alfabetoMay [] [\A \Á \B \C \D \E \É \F \G \H \I \Í \J \K \L \M \N \Ñ \O \Ó \P \Q \R \S \T \U \Ú \Ü \V \W \X \Y \Z \0 \1 \2 \3 \4 \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9])

(defn tabla [] (merge (into {} (map vector (alfabetoMin) (range (count (alfabetoMin))))) (into {} (map vector (alfabetoMay) (range 100 (+ 100 (count (alfabetoMay))))))))

(defn buscar [s] (get (tabla) s))

(defn buscar-posiciones [xs] (map (fn [c] (if (> (buscar c) (count (alfabetoMin))) (- (buscar c) 100) (buscar c))) xs))

(defn matriz [xs acc ys] (if (int? (/ (count ys) (count xs))) (vec (map vec (partition-all acc ys))) (conj (vec (drop-last (partition-all acc ys))) (concat (last (partition-all acc ys)) (repeat acc "")))))

(defn ordenar [xs ys] (keys (sort-by val < (zipmap (vec (apply map vector (matriz xs (count xs) ys))) (buscar-posiciones xs)))))
(defn descifrado [xs ys] (apply map vector (keys (sort-by val < (zipmap (matriz xs (count xs) ys) (buscar-posiciones xs))))))

(defn leer [f] (let [s (slurp f)] s))

(defn escribir [f content] (spit f content))

(defn init [operacion xs input output] (cond (< (count xs) 7) "Clave no válida" (some nil? (map (fn [c] (buscar c)) xs)) "Clave no válida" (= operacion "cifrado") (escribir output (apply str (flatten (ordenar xs (leer input)))))
        (= operacion "descifrado") (escribir output (apply str (flatten (descifrado xs (leer input))))) :else "Parámetros no válidos"))