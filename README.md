# plf08

## Descripción
Este proyecto contiene dos aplicaciones de cifrado y descifrado
- Cuadrado de Polibio 
- Transposición columnar simple

## Ejecución
### Cuadrado de Polibio 
    lein run operacion leer.txt escribir.txt

#### Ejemplo
    lein run "cifrado" "./resources/polibio-des.txt" "./resources/polibio-cif.txt"

### Transposición columnar simple
    lein run operacion clave leer.txt escribir.txt

#### Ejemplo
    lein run "cifrado" "canciones" "./resources/transposicion-leer.txt" "./resources/transposicion-escribir.txt"